#AWS ECR
## PreRequiste
If you do not have the latest AWS CLI and Docker installed and ready to use, use the following steps to install both of these tools.

#Update the system.
sudo apt-get update -y


################################################################################################################
########################################### AWS CLI INSTALLATION ###############################################
################################################################################################################
#Install AWS Cli, Before install unzip packages on ubuntu:
sudo apt-get install unzip -y 
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
sudo ./aws/install

#################################################################################################################
############################################## DOCKER INSTALLATION ##############################################
#################################################################################################################
# Install Docker in Linux:
#!/bin/bash

# Add Docker's official GPG key:
sudo apt-get update -y
sudo apt-get install ca-certificates curl
sudo install -m 0755 -d /etc/apt/keyrings
sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc
sudo chmod a+r /etc/apt/keyrings/docker.asc

# Add the repository to Apt sources:
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

#Update the system:
sudo apt-get update -y

#Install the linux version:
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin -y


################################################################################################################
############################################ IAM PERMISSION ####################################################
################################################################################################################

ECR Read and Write Access:
IAM policy grants the required permissions for pushing an image to all repositories:

{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "ecr:CompleteLayerUpload",
                "ecr:GetAuthorizationToken",
                "ecr:UploadLayerPart",
                "ecr:InitiateLayerUpload",
                "ecr:BatchCheckLayerAvailability",
                "ecr:PutImage"
            ],
            "Resource": "*"
        }
    ]
}

################################################################################################################
#################################################   Create Docker images   ########################################
###############################################################################################################

# Create a Dockerfile:

# Build the Dockerfile:



# Authentication token to the docker login command
ap-south-1 (Mumbai)
aws ecr get-login-password --region ap-south-1 | docker login --username AWS --password-stdin 851725166437.dkr.ecr.ap-south-1.amazonaws.com

us-east-1 (N.Virginia)
aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin 851725166437.dkr.ecr.us-east-1.amazonaws.com


# Create a repository:
ap-south-1 (Mumbai)

aws ecr create-repository \
    --repository-name hello-world \
    --region ap-south-1

us-east-1 (N.Virginia):

aws ecr create-repository \
    --repository-name rahulnv \
    --region us-east-1

# Docker Images:
docker images

# TAG an image to Amazon ECR:
ap-south-1 (Mumbai)

docker tag hello-world:latest 851725166437.dkr.ecr.ap-south-1.amazonaws.com/hello-world

us-east-1 (N.Virginia):

docker tag rahulnv:latest 851725166437.dkr.ecr.us-east-1.amazonaws.com/rahulnv

# Push Images to Amazon ECR:
ap-south-1 (Mumbai)

docker push 851725166437.dkr.ecr.ap-south-1.amazonaws.com/hello-world

us-east-1 (N.Virginia):

docker push 851725166437.dkr.ecr.us-east-1.amazonaws.com/rahulnv

